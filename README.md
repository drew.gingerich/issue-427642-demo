This repository demonstrates behavior related to [GitLab.com issue 427642: Display prefilled variables:options when includes populate jobs](https://gitlab.com/gitlab-org/gitlab/-/issues/427642).

It presents 3 cases to help provide bounds to the behavior. Each case can be examined by updating the `include` in `.gitlab-ci.yml` to point at `template.yml` file for that case. To observe the behavior, create a new pipeline with the web UI and see if the prefilled variables show up or not.

The desired behavior is to show the [selectable prefilled variables in the Web UI](https://docs.gitlab.com/ee/ci/pipelines/index.html#configure-a-list-of-selectable-prefilled-variable-values). The issue is that they do not appear when the only jobs in the pipeline are included, and the inclusion has rules.

The _conditional-include_ case demonstrates the issue. The selectable prefilled variables do not appear when creating a new pipeline via the web UI.

The _conditional-include-workaround_ case shows a known workaround for the issue, which is to add a dummy job that is always present. Because stages are defined in the included files, this example sets the stage of the dummy job to `.post` because this stage always exists.

The _unconditional-include_ case shows that the selectable prefilled variables show up, as expected, when the inclusion doesn't use rules.
